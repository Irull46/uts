import React from 'react';
import { View, StyleSheet, Image } from 'react-native';
import { Home, Search, Plus, Market, User } from '../assets';

const Navigation = () => {
    return (
        <View>
            <View style={styles.box}>
                <Image source={Home} style={styles.Home} />
                <Image source={Search} style={styles.Search} />
                <Image source={Plus} style={styles.Plus} />
                <Image source={Market} style={styles.Market} />
                <Image source={User} style={styles.User} />
            </View>
        </View>
    );
};

export default Navigation;

const styles = StyleSheet.create({
    box: {
        width: 400,
        height: 55,
        backgroundColor: '#000000',
        flexDirection: 'row',
        position: 'absolute',
        bottom: 45,
    },
    Home: {
        marginLeft: 24,
        marginTop: 13,
        width: 20,
        height: 20,
    },
    Search: {
        marginLeft: 51,
        marginTop: 13,
        width: 22,
        height: 22,
    },
    Plus: {
        marginLeft: 51,
        marginTop: 13,
        width: 22,
        height: 22,
    },
    Market: {
        marginLeft: 51,
        marginTop: 13,
        width: 19.5,
        height: 22,
    },
    User: {
        marginLeft: 51,
        marginTop: 13,
        width: 24,
        height: 24,
        borderRadius: 24/2,
    },
});