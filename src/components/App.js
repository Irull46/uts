import React from 'react';
import { View, ScrollView } from 'react-native';
import { Header, Stories, Content, Navigation } from '../components';

const App = () => {
  return (
    <View style={{backgroundColor: '#000000'}}>
      <Header />
      <ScrollView showsVerticalScrollIndicator={false}>
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          <Stories />
        </ScrollView>
        <Content />
        <Content />
        <Content />
        <Content />
      </ScrollView>
      <Navigation />
    </View>
  );
};

export default App;
