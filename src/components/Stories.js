import React from 'react';
import { View, Image, StyleSheet, Text } from 'react-native';
import { Ring, User, Foto1, Foto2, Foto3, Foto4 } from '../assets';

const PropsStorie = props => {
    return (
        <View>
            <View style={styles.Storie2}>
                <Image source={props.ring} style={styles.Ring} />
                <Image source={props.foto} style={styles.Foto} />
            </View>
            <Text style={styles.Cerita}>{props.teks}</Text>
        </View>
    );
};

const Stories = () => {
    return (
        <View style={styles.box}>
            <View style={{flexDirection: 'row'}}>
                <View style={styles.Storie1}>
                    <View style={styles.RingPlus} />
                    <Image source={User} style={styles.User} />
                    <Text style={styles.Plus}>+</Text>
                </View>
                <PropsStorie ring={Ring} foto={Foto1} teks='Reemar' />
                <PropsStorie ring={Ring} foto={Foto2} teks='Resilya' />
                <PropsStorie ring={Ring} foto={Foto3} teks='Natalya' />
                <PropsStorie ring={Ring} foto={Foto4} teks='Natasya' />
            </View>
        </View>
    );
};

export default Stories;

const styles = StyleSheet.create({
    box: {
        borderWidth: 1,
        borderBottomColor: '#2C3E50',
        paddingBottom: 6,
    },
    
    Storie1: {
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 15,
    },
    RingPlus: {
        width: 57,
        height: 57,
        borderWidth: 1,
        borderRadius: 57/2,
        borderColor: '#7F8C8D',
        position: 'relative',
    },
    Plus: {
        position: 'absolute',
        color: '#FFFFFF',
        backgroundColor: '#3498DB',
        width: 21,
        height: 21,
        borderRadius: 25,
        right: 1,
        bottom: 19,
        fontSize: 13,
        paddingLeft: 6,
        borderWidth: 3,
    },
    User: {
        width: 55,
        height: 55,
        borderRadius: 55/2,
        position: 'absolute',
    },

    Storie2: {
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 15,
        marginTop: 11,
    },
    Ring: {
        width: 65,
        height: 65,
    },
    Foto: {
        width: 54,
        height: 54,
        borderRadius: 54/2,
        position: 'absolute',
    },
    Cerita: {
        marginLeft: 17,
        marginTop: 8,
        color: '#FFFFFF',
        fontSize: 12,
        textAlign: 'center',
        maxWidth: 60,
    },
});
