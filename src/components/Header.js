import React from 'react';
import { View, StyleSheet, Image } from 'react-native';
import { Instagram, Love, Pesawat } from '../assets';

const Header = () => {
    return (
        <View style={{flexDirection: 'row'}}>
            <View style={styles.box}>
                <Image source={Instagram} style={styles.Instagram} />
                <Image source={Love} style={styles.Love} />
                <Image source={Pesawat} style={styles.Pesawat} />
            </View>
        </View>
    );
};

export default Header;

const styles = StyleSheet.create({
    box: {
        flex: 1,
        height: 55,
        backgroundColor: '#000000',
        flexDirection: 'row',
    },
    Instagram: {
        marginLeft: 15,
        marginTop: 15,
        width: 110,
        height: 31,
    },
    Love: {
        marginLeft: 143,
        marginTop: 17,
        width: 23.5,
        height: 21,
    },
    Pesawat: {
        marginLeft: 25,
        marginTop: 17,
        width: 25,
        height: 21,
    },
});