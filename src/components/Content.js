import React, { useState } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import { Ring, User, Foto1, Foto2, Foto3, Foto4, Titik, Developer, Love, Comment, Pesawat, Save } from '../assets';

const Content = () => {
    const [like, setLike] = useState(0);
    return (
        <View>
            <View style={styles.box1}>
                <View style={styles.profile}>
                    <Image source={Ring} style={styles.Ring} />
                    <Image source={User} style={styles.User} />
                </View>
                <Text style={styles.namasaya}>eenkanam</Text>
                <Image source={Titik} style={styles.Titik} />
            </View>
            <Image source={Developer} style={styles.Developer} />
            <View style={styles.box2}>
                <TouchableOpacity onPress={() => setLike(like + 1)}>
                    <Image source={Love} style={styles.Love} />
                </TouchableOpacity>
                <Image source={Comment} style={styles.Comment} />
                <Image source={Pesawat} style={styles.Pesawat} />
                <Image source={Save} style={styles.Save} />
            </View>
            <Text style={styles.Like}>
                <Text>Disukai oleh </Text>
                <Text style={{fontWeight: 'bold'}}>reemar </Text>
                <Text>dan </Text>
                <Text style={{fontWeight: 'bold'}}>{like} lainnya</Text>
            </Text>
            <Text style={styles.Description}>
                <Text style={{fontWeight: 'bold'}}>eenkanam </Text>
                <Text>Mulai belajar React Native, gimana menurut kalian?</Text>
            </Text>
            <Text style={styles.Komentar}>Lihat semua 460 komentar</Text>
            <Text style={styles.Jam}>1 hari yang lalu</Text>
        </View>
    );
};

export default Content;

const styles = StyleSheet.create({
    box1: {
        flexDirection: 'row',
        marginLeft: 10,
        marginTop: 10,
    },
    profile: {
        alignItems: 'center',
        justifyContent: 'center',
    },

    Ring: {
        width: 36,
        height: 36,
    },
    User: {
        width: 26,
        height: 26,
        borderRadius: 26/2,
        position: 'absolute',
    },
    namasaya: {
        color: '#FFFFFF',
        fontWeight: 'bold',
        marginLeft: 6,
        marginTop: 8,
    },
    Titik: {
        width: 3,
        height: 14,
        marginLeft: 220,
        marginTop: 13,
    },

    Developer: {
        marginTop: 10,
        width: 365,
        height: 280,
    },

    box2: {
        flexDirection: 'row',
        marginLeft: 12,
        marginTop: 8,
    },
    Love: {
        width: 21,
        height: 19,
    },
    Comment: {
        width: 19,
        height: 19,
        marginLeft: 12,
    },
    Pesawat: {
        width: 21,
        height: 18,
        marginTop: 1,
        marginLeft: 12,
    },
    Save: {
        width: 16,
        height: 19,
        marginLeft: 230,
    },
    Like: {
        color: '#FFFFFF',
        fontSize: 14,
        marginLeft: 12,
        marginTop: 10,
    },
    Description: {
        color: '#FFFFFF',
        fontSize: 14,
        marginLeft: 12,
    },
    Komentar: {
        color: '#7F8C8D',
        fontSize: 14,
        marginLeft: 12,
        marginTop: 2,
    },
    Jam: {
        color: '#7F8C8D',
        fontSize: 11,
        marginLeft: 12,
        marginTop: 2,
    },
});
