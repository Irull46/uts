import Comment from './comment.png';
import Home from './home.png';
import Instagram from './instagram.png';
import Love from './love.png';
import Market from './market.png';
import Pesawat from './pesawat.png';
import Plus from './plus.png';
import Save from './save.png';
import Search from './search.png';
import Titik from './titik.png';
import Ring from './ring.png';
import User from './user.jpg';
import Foto1 from './foto1.jpg';
import Foto2 from './foto2.png';
import Foto3 from './foto3.jpg';
import Foto4 from './foto4.jpg';
import Developer from './developer.jpg';

export { Comment, Home, Instagram, Love, Market, Pesawat, Plus, Save, Search, Titik, Ring, User, Foto1, Foto2, Foto3, Foto4, Developer };